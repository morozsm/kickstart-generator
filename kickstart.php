<?php

$rhel=($_GET[rhel]);
$ip=($_GET[ip]);
$gateway=($_GET[gateway]);
$netmask=($_GET[netmask]);
$nameserver=($_GET[nameserver]);
$hostname=($_GET[hostname]);
$swapsize=($_GET[swapsize]);
#$password=(crypt($_GET[password]));
$password=($_GET[password]);

echo "
auth  --useshadow  --enablemd5
bootloader --location=mbr 
clearpart --all
firewall --disabled
selinux --disabled
firstboot --disable
keyboard us
lang en_US.UTF-8
url --url=http://yum.realitychecknetwork.com/mirror/centos/6/os/x86_64
repo --name=rcn-release-centos-updates --baseurl=http://yum.realitychecknetwork.com/mirror/centos/6/updates/x86_64
repo --name=rcn-release-epel --baseurl=http://yum.realitychecknetwork.com/mirror/epel/6/x86_64/
repo --name=rcn-release-epel-puppet --baseurl=http://tmz.fedorapeople.org/repo/puppet/epel/6/x86_64/


network --device=eth0 --bootproto=static --ip=$ip --netmask=$netmask --nameserver=$nameserver --gateway=$gateway --hostname=$hostname --noipv6 
services --enabled=sshd,nscd,netfs,ntpd,sendmail --disabled=rpcgssd,rpcidmapd,rpcsvcgssd,portmap,nfslock,nfs,avahi-daemon,avahi-dnsconfd,gpm,xfs,cups,iscsi,iscsid

# poweroff
reboot

#rootpw --iscrypted $password

rootpw $password

# Do not configure the X Window System
skipx
# System timezone
timezone  America/New_York
# Install OS instead of upgrade
install
# Allow anaconda to partition the system as needed
clearpart --all
zerombr
part swap --asprimary --fstype=swap --size=$swapsize --label=SWAP
part    / --asprimary --fstype=ext3 --size=1024 --label=ROOT --grow --fsoptions=defaults,noatime

%packages --excludedocs --nobase
@Core
wget
ntp
rsync
screen
sysstat
man
system-config-securitylevel-tui
nano
puppet
nscd
openssh-server
nfs-utils
sendmail
perl

%post
#!/bin/bash
/usr/sbin/lokkit -q --disabled --removemodule=ip_conntrack_netbios_ns --selinux='disabled'
mkdir /root/.ssh
chmod 700 /root/.ssh
wget -q http://yum.realitychecknetwork.com/misc/ssh_key.txt -O /root/.ssh/authorized_keys
sed -i 's/^#PermitRootLogin yes/PermitRootLogin without-password/g' /etc/ssh/sshd_config
%end";
?>

